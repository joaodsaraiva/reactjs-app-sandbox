import React from 'react';

class Categories extends React.Component {

    constructor(props)
    {
        super(props);
        this.state = {
            categories: this.props.categories
        }
    }

    loadCategories = (uniqueCategories) =>
    {

        this.state = {
            categories: uniqueCategories
        }

    }

    changeCategory = () => {

        console.log("click")
        console.log(this.state.categories)

        // TODO update the list



    }

    render() {

        let categories = this.props.categories;
        let uniqueCategories = Array.from(new Set(categories));
        this.loadCategories(uniqueCategories);

        return (
            <li className="Categories" key={this.props.id} onClick={this.changeCategory} > {uniqueCategories.join(' / ')} </li>
        )
    }
}

export default Categories;