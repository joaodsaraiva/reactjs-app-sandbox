import React from "react";

class Search extends React.Component {

  filterList (event){

      let initialItems = [ "Apples",  "Broccoli", ];

      // Ir buscar dados que já estão listados ...

      initialItems = initialItems.filter(function(item){

       // loaded items
       console.log(initialItems);

        return item.toLowerCase().search(
        event.target.value.toLowerCase()) !== -1;

    });

    //this.setState({items: updatedList});

  }

  componentWillMount (){

  }

  render (){
    return (
      <div className="Search">
        <input type="text" placeholder="Search your item" onChange={this.filterList}/>
      </div>
    );
  }
}

export default Search;
