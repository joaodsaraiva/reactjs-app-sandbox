import React from 'react';
import axios from "axios";

class AppsList extends React.Component  {

    constructor(props)
    {
        super(props);

        this.state = {
            fromIndex:0,
            toIndex:3,
            apps: []
        };

        this.filterTodo = this.filterTodo.bind(this);
    }

    componentDidMount()
    {

        axios.get('apps.json')
            .then(res => {

                const apps = res.data;

                this.setState({
                    apps: apps.map(i => i.title)
                });

            })

    }

    filterTodo(e)
    {

        console.log("filterTodo")

        let updatedList = this.props.apps.map(i => i.title);

        updatedList = updatedList.filter((item =>{
            return item.toLowerCase().search(
                e.target.value.toLowerCase()) !== -1;
        }) );

        this.setState({
            apps: updatedList,
        });

    }

    next = () => {

        let maxItems = this.state.apps.length;

        if (this.state.toIndex >= maxItems) return;

        this.setState({
            fromIndex: this.state.fromIndex + 3,
            toIndex: this.state.toIndex + 3
        });
    }

    previous = () => {

        if (this.state.fromIndex <= 0) return;

        this.setState({
            fromIndex: this.state.fromIndex - 3,
            toIndex: this.state.toIndex - 3
        });
    }

    render()
    {

        return (
            <div className="AppsList">

                <div className="Search">
                    <input  type="text" placeholder="Search your item" onChange={this.filterTodo} />
                </div>

                <ul>
                    {this.state.apps.slice(this.state.fromIndex, this.state.toIndex).map((todo) =>
                        {
                            return (<li key={Math.floor(Math.random() * 10000) + 1}>{todo}</li>);
                        }
                    )}
                </ul>

                <div>
                    <button className="btn" onClick={this.previous} title="Previous list">&lt;</button>
                    <button className="btn" onClick={this.next} title="Next List">&gt;</button>
                </div>

            </div>
        );
    }

}

export default AppsList;