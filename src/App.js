import React from 'react';
import axios from 'axios';
import './App.css';

// TODO Filter by category

import Categories from './components/categories';
import AppsList from './components/appslist';

class App extends React.Component {

    state = {
        apps: [],
        categories: []
    }

    componentDidMount() {
        axios.get('apps.json')
            .then(res => {

                const apps = res.data;
                this.setState({ apps });

            })
    }

    render() {

        let categories = this.state.apps.map(i => i.category);           // map
        let mergedCategories = [].concat.apply([], categories);  // merge
        let uniqueCategories = [...new Set(mergedCategories)];           // remove duplicates

        return (
            <div className="App">

                <AppsList className="AppsList" apps={this.state.apps} />
                <Categories categories={uniqueCategories} />

            </div>
        );

    }

}

export default App;